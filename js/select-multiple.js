jQuery(function($) {

    $('#search').on('focus', function() {
        if ($(this).parent().siblings('.selectbox-dropdown').prop('hidden', false)) {
            $(this).parent().siblings('.selectbox-dropdown').slideUp('fast');
            $(this).parent().find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
        }
    });

    $('#search').on('keyup', function() {

        var search = $('#search').val();

        if (search.length >= 3) {

            $.post(
                'SearchDb.php',
                {
                    searchTerm: search
                },
                function(response) {

                    $('.selectbox-dropdown').html(response);
                    $('.selectbox-dropdown').slideDown('fast');
                    $('.expand-selectbox-dropdown').children('i').removeClass('fa-angle-down').addClass('fa-angle-up');
                }
            );
        }
        if (search.length == 0) {
            $('.selectbox-dropdown').html('');
            $('.selectbox-dropdown').slideUp('fast');
            $('.selectbox-dropdown').siblings('.input-group').find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
        }
    });

    $('.expand-selectbox-dropdown').on('click', function() {

        if ($(this).children('i').hasClass('fa-angle-down')) {
            if (!$(this).parent().siblings('div.selectbox-dropdown').is(':empty')) {
                $(this).parent().siblings('div.selectbox-dropdown').slideDown('fast');
                $(this).children('i').removeClass('fa-angle-down').addClass('fa-angle-up');
            }
        }
        else {
            $(this).parent().siblings('.selectbox-dropdown').slideUp('fast');
            $(this).children('i').removeClass('fa-angle-up').addClass('fa-angle-down');
        }
    });
    var selectboxDatas = [];
    $(document).on('click', '.dropdown-option', function() {

        var data = $(this).text();
        selectboxDatas.push(data);
        var tag = $('.select-tag');
        var string = tag.children('.select-tag-text').text();
        if (string.length == 0) {
            tag.children('.select-tag-text').text(data);
            tag.fadeIn('fast');
        }
        else {
            var newTag = $('.select-tag').first().clone(true);
            newTag.hide();
            newTag.children('.select-tag-text').text(data);
            // alert(newTag.children('.select-tag-text').text());
            $('.selectbox-tag-block').append(newTag);
            newTag.fadeIn('fast');
        }

        $(this).remove();
    });

     $('#searchForm').on('submit', function(e) {
         e.preventDefault(e);
         $(this).find('#searchBtn').prop('disabled', true);
         var items = [];
         $(this).find('.selectbox-tag-block').children('.select-tag').each(function() {
             items.push($(this).find('.select-tag-text').text());
         });
         $.post(
             'DbSave.php',
             {
                 data: items,
             },
             function(response) {
                 $('.msgbox').text(response.msg);
                 if (response.success == true) {
                     $('.msgbox').addClass('success');
                 }
                 else {
                     $('.msgbox').addClass('error');
                 }
                 $('.msgbox').fadeIn('fast').fadeOut(3000);
                 $('#searchBtn').prop('disabled', false);
             },
             'json'
         );
     });
});
